package com.bibao.boot.jpaallocationsize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.bibao.boot.dao"})
@EntityScan(basePackages = {"com.bibao.boot.entity"})
@ComponentScan("com.bibao.boot")
public class JpaAllocationsizeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaAllocationsizeApplication.class, args);
	}

}
