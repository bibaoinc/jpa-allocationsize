drop table PASS_BOOK;

drop sequence PASS_BOOK_SEQ;

create table PASS_BOOK (
	id number(5) primary key,
	plain_code varchar(100) not null,
	cipher_code varchar(100) not null
);

create sequence PASS_BOOK_SEQ
start with 1
increment by 5;